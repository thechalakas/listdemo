import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <Text>Changes you make will automatically reload.</Text>
        <Text>Shake your phone to open the developer menu.</Text>

        <FlatList
      data={[{key: 'Batman', realname: 'Bruce Wayne'}, {key: 'Superman',realname: 'Bruce Wayne'}, {key: 'Wonder Woman',realname: 'Bruce Wayne'}, {key: 'Flash',realname: 'Bruce Wayne'}]}
      renderItem={({item}) => (
                            <View>
                              <Text>{item.key}</Text>
                              <Text>Hello There </Text>
                              <Text>{item.realname} </Text>
                            </View>
                    
                              )
                  }
    />        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
